package ParseCSV;

# Parse the input including quoted fields...

sub	parseCSV
{
	my	$delimiter = shift(@_);
	my	@cInfo = split(/${delimiter}/, $_[0]);	# parse array looking for delimited fields...
	my	@qInfo = split(/"/, $_[0]);		# parse array looking for quote-separated fields...
	my	@result = qw();				# the final result... This is what is going to get returned to the caller...
	my	$qI = 0;				# Index for the quote-separated array...

	for (my $i = 0; $i < scalar(@cInfo); $i++)	{		# Check each of the delimited fields...
		if ($cInfo[$i] =~ m/"/)	{				# If this field has a quote...
			my	$t = $cInfo[$i];
			my	$c = $t =~ tr/"//;			# Count and replace the quotes

			if ($c == 2)	{				# If it's 2 then we use the entire field minus the quotes...
				$cInfo[$i] =~ s/"//g;
				push(@result, ($cInfo[$i]));
				$qI += 2;
			} else	{					# elsewise we skip all the fields until we find another quote...
				do	{
					$i++;
				} while ($cInfo[$i] !~ m/"/);

				push(@result, ($qInfo[++$qI]));		# ...and we capture every other quote-sepated field..
				$qI++;
			}
		} else	{						# No quotes in this field, we can use it as is...
			push(@result, ($cInfo[$i]));
		}
	}

	if ($warnMe)	{
		printf("Field has quoted delimiters\n")		unless ($#qInfo == 0);	# Alert the user that this might be a problem...
	}


	return(@result);
}

1;

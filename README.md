Purpose
-------

This is a perl module which parses CSV files extracting quoted fields. Normally we can use the `split()` function
to extract fields from CSV input, but in the case where we have quoted fields containing commas (or other delimeters)
that should be protected, this module can be used. This version of the code matches only a single type of quoted
fields (using double quotes), so you can't mix and match the types of quotes you're using.

Usage
-----

The module needs to be installed on your system or used from a local directory whilst ensuring that perl is checking
that directory for modules. There's only one function: `parseCSV`, which takes 2 parameters:

 - The delimeter in use
 - The line of input to be parsed

Using this input file:

	this,is,the,first,line
	this, is, the, second, line
	"this is", the, "third line"
	"this,is", the, "fourth,line"
	"this,is,the,last,line"

This perl script:

	#! /usr/bin/perl

		use	strict;
		require	ParseCSV;

		while (<STDIN>)	{
			my	@parsedInput;

			chomp($_);
			@parsedInput = ParseCSV::parseCSV(',', $_);

			printf("Parsed these fields from: '%s'\n", $_);

			for (my $i = 0; $i <= $#parsedInput; $i++)	{
				printf("%2d: '%s'\n", $i, $parsedInput[$i]);
			}

			printf("\n");
		}


	exit(0);

Produces this output:

	Parsed these fields from: 'this,is,the,first,line'
	 0: 'this'
	 1: 'is'
	 2: 'the'
	 3: 'first'
	 4: 'line'

	Parsed these fields from: 'this, is, the, second, line'
	 0: 'this'
	 1: ' is'
	 2: ' the'
	 3: ' second'
	 4: ' line'

	Parsed these fields from: '"this is", the, "third line"'
	 0: 'this is'
	 1: ' the'
	 2: ' third line'

	Parsed these fields from: '"this,is", the, "fourth,line"'
	 0: 'this,is'
	 1: ' the'
	 2: 'fourth,line'

	Parsed these fields from: '"this,is,the,last,line"'
	 0: 'this,is,the,last,line'

